const connection = require("../../config/db");
const bcryptjs=require('bcryptjs')
module.exports= app => {
	app.get('/', (req,res) => {

		if (req.session.loggedin){
			res.render('../views/index.ejs', {
				login:true,
				name: req.session.name
			});
		} else {
			res.render('../views/index.ejs', {
				login:false,
				name: "por favor inicie sesion"
			});
		}
	})

	app.get('/login', (req,res)=>{
		res.render('../views/login.ejs')
	})

	app.get('/mascotas', (req,res)=>{
		res.render('../views/index_mascotas.ejs')
	})

	app.get('/register', (req,res)=>{
		res.render('../views/register.ejs')
	})

	app.get('/logout', (req,res)=> {
		req.session.destroy(() => {
			res.redirect('/');
		})
	})

	

	app.post('/register', async (req,res) => {
		const {user,name,rol,pass} = req.body;
		console.log(req.body);
		let passwordHash = await bcryptjs.hash(pass, 8);
		connection.query ("INSERT INTO users SET ?", {
			user:user,
			name:name,
			rol:rol,
			pass:passwordHash

		}, async (error, results)=>{
			if (error){
				console.log(error);
			} else{
				res.render('../views/register.ejs', {
					alert: true,
					alertTitle: "Registration",
					alertMessage: "successful Registration",
					alertIcon: "success",
					showConfirmButton: false,
					timer: 1500,
					ruta:''
				});
			}
		})
	})

	app.post('/auth', async (req,res)=>{
		const {user,pass} = req.body;
		let passwordHash = await bcryptjs.hash(pass, 8);

		if(user && pass){
			connection.query('SELECT * FROM users WHERE user = ?', [user], async(err, results) =>{
				console.log(results);
				if(results.length === 0 || !(await bcryptjs.compare(pass, results[0].pass))){
						res.render('../views/login.ejs', {
							alert: true,
							alertTitle: "F",
							alertMessage: "Aceso denegado",
							alertIcon: "error",
							showConfirmButton: true,
							timer: 1500,
							ruta: '/login'
							})

				} else {
						req.session.loggedin = true;
						req.session.name = results[0].name;
						res.render('../views/login.ejs', {
							alert:true,
							alertTitle: "Bienvenido",
							alertMessage: "acceso permitido",
							alertIcon: "success",
							showConfirmButton: false,
							timer: 1500,
							ruta: '/'
						})
				}

				
			})
		}
	})

}